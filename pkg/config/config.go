package config

import (
	"encoding/json"
	"os"

	env "github.com/Netflix/go-env"
	log "github.com/sirupsen/logrus"
)

// DatabaseConfig by gorm settings
type DatabaseConfig struct {
	Driver string `env:"DATABASE_DRIVER" json:"driver"`
	Path   string `env:"DATABASE_PATH" json:"path"`
}

// Config contains the complete service configuration
type Config struct {
	Database DatabaseConfig `json:"database"`

	OperatorToken string `env:"OPERATOR_TOKEN" json:"operator_token"`
}

// NewTestConfig return a config object with test settings
func NewTestConfig() *Config {
	return &Config{
		Database: DatabaseConfig{
			Driver: "sqlite3",
			Path:   "/tmp/test.db",
		},
		OperatorToken: "supersecret",
	}
}

// ReadConfig reads a json file and overwrite with ENV vars
func ReadConfig(file string) *Config {
	var config Config

	if fileExists(file) {
		fileContent, _ := os.Open(file)

		if err := json.NewDecoder(fileContent).Decode(&config); err != nil {
			log.Fatal(err)
		}
	}

	// Override ENVs
	_, err := env.UnmarshalFromEnviron(&config)
	if err != nil {
		log.Fatal(err)
	}

	if config.Database.Driver == "" {
		log.Fatal("Need DATABASE_DRIVER env var")
	}

	if config.Database.Driver != "sqlite3" && config.Database.Driver != "mysql" && config.Database.Driver != "postgres" {
		log.Fatal("Only supported driver are sqlite3, mysql, postgres")
	}

	if config.Database.Path == "" {
		log.Fatal("Need DATABASE_PATH env var")
	}

	return &config
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}
