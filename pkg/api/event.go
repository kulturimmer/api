package api

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"github.com/go-chi/chi"
	validator "github.com/go-playground/validator/v10"
	"github.com/google/uuid"

	"gitlab.com/kulturimmer/api/pkg/common/router"
	session "gitlab.com/kulturimmer/api/pkg/context"
	"gitlab.com/kulturimmer/api/pkg/models"
)

type EventRequest struct {
	Title       string `json:"title" validate:"required,min=5,max=200"`
	Description string `json:"description" validate:"omitempty,max=200"`

	ImageURL string `json:"image_url" validate:"omitempty,url"`

	EventStart    string `json:"event_start" validate:"required"`
	WithStartTime bool   `json:"with_starttime"  validate:"-"`

	Platform string `json:"platform" validate:"omitempty,max=40"`
	URL      string `json:"url" validate:"omitempty,url"`

	Category string `json:"category" validate:"omitempty,max=40"`
}

// Get Events
func (api *API) getEvents(w http.ResponseWriter, r *http.Request) error {
	ctx := r.Context()

	log := session.GetLogger(ctx)
	log.Info("Get Events")

	db := pagination(api.db, r)
	db = dateRange(api.db, r)

	var events []models.Event
	if res := db.Order("event_start").Find(&events); res.Error != nil {
		if !res.RecordNotFound() {
			return router.HandleSQLError(res.Error)
		}
	}

	return router.SendList(w, http.StatusOK, events)
}

// Get Event
func (api *API) getEvent(w http.ResponseWriter, r *http.Request) error {
	ctx := r.Context()

	log := session.GetLogger(ctx)
	log.Info("Get Event")

	event := session.GetEvent(ctx)

	return router.SendObject(w, http.StatusOK, event)
}

func (api *API) createEvent(w http.ResponseWriter, r *http.Request) error {
	ctx := r.Context()

	log := session.GetLogger(ctx)
	log.Info("Create Event")

	request := EventRequest{}

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return router.BadRequestError("bad payload").WithInternalError(err)
	}

	validate := validator.New()
	if err := validate.Struct(&request); err != nil {
		return router.BadRequestError("bad payload").WithInternalError(err)
	}

	eventStart, err := time.Parse(time.RFC3339, request.EventStart)
	if err != nil {
		return router.BadRequestError("bad payload").WithInternalError(err)
	}

	event := models.Event{
		Title:         request.Title,
		Description:   request.Description,
		ImageURL:      request.ImageURL,
		EventStart:    eventStart,
		WithStartTime: request.WithStartTime,
		Platform:      request.Platform,
		URL:           request.URL,
		Category:      request.Category,
	}

	// create
	if err := api.db.Create(&event).Error; err != nil {
		return router.HandleSQLError(err)
	}

	return router.SendObject(w, http.StatusOK, event)
}

func (api *API) updateEvent(w http.ResponseWriter, r *http.Request) error {
	ctx := r.Context()

	log := session.GetLogger(ctx)
	log.Info("Update Event")

	event := session.GetEvent(ctx)
	request := EventRequest{}

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return router.BadRequestError("bad payload").WithInternalError(err)
	}

	validate := validator.New()
	if err := validate.Struct(request); err != nil {
		return router.BadRequestError("bad payload").WithJSONError(err)
	}

	eventStart, err := time.Parse(time.RFC3339, request.EventStart)
	if err != nil {
		return router.BadRequestError("bad payload").WithInternalError(err)
	}

	event.Title = request.Title
	event.Description = request.Description
	event.ImageURL = request.ImageURL
	event.EventStart = eventStart
	event.WithStartTime = request.WithStartTime
	event.Platform = request.Platform
	event.URL = request.URL
	event.Category = request.Category

	// update
	if err := api.db.Save(&event).Error; err != nil {
		return router.HandleSQLError(err)
	}

	return router.SendObject(w, http.StatusOK, event)
}

// withEvent by Middleware
func (api *API) withEvent(w http.ResponseWriter, r *http.Request) (context.Context, error) {
	ctx := r.Context()

	id := chi.URLParam(r, "eventID")

	if _, err := uuid.Parse(id); err != nil {
		return nil, router.BadRequestError("invalid event id").WithInternalError(err)
	}

	query := models.Event{
		ID: id,
	}

	var event models.Event
	if res := api.db.Where(query).First(&event); res.Error != nil {
		if !res.RecordNotFound() {
			return nil, router.HandleSQLError(res.Error)
		}

		return nil, router.NotFoundError("event not found")
	}

	ctx = session.WithEvent(ctx, &event)

	return ctx, nil
}
