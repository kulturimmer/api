package api

import (
	"net/http"
	"time"

	"github.com/jinzhu/gorm"

	"gitlab.com/kulturimmer/api/pkg/common/router"
)

const (
	layoutDateRange = "2006-01-02"
)

func pagination(db *gorm.DB, r *http.Request) *gorm.DB {
	perPage := router.QueryParamInt(r, "per_page")
	page := router.QueryParamInt(r, "page")

	// limit
	if perPage <= 0 || perPage > 50 {
		perPage = 20
	}

	db = db.Limit(perPage)

	// filter per page
	if page > 0 {
		offset := perPage * page
		db = db.Offset(offset)
	}

	return db
}

func dateRange(db *gorm.DB, r *http.Request) *gorm.DB {
	start := router.QueryParamString(r, "start")
	end := router.QueryParamString(r, "end")

	if start == "" {
		return db
	}

	startDate, err := time.Parse(layoutDateRange, start)
	if err != nil {
		return db
	}

	if end == "" {
		return db.Where("date_start > ?", startDate)
	}

	endDate, err := time.Parse(layoutDateRange, end)
	if err != nil {
		return db
	}

	if endDate.Before(startDate) {
		return db
	}

	return db.Where("date_start BETWEEN ? AND ?", startDate, endDate)
}
