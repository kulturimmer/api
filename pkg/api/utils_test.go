package api

import (
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	logrus "github.com/sirupsen/logrus"

	"gitlab.com/kulturimmer/api/pkg/config"
	"gitlab.com/kulturimmer/api/pkg/models"
)

type RouteTest struct {
	DB     *gorm.DB
	Config *config.Config
	T      *testing.T
}

type EndpointTest struct {
	Method string
	URL    string
	Body   io.Reader
	Token  *string
}

func NewRouteTest(t *testing.T) *RouteTest {
	config := config.NewTestConfig()

	if _, err := os.Stat("/tmp/test.db"); !os.IsNotExist(err) {
		os.Remove("/tmp/test.db")
	}

	db, _ := gorm.Open(config.Database.Driver, config.Database.Path)

	models.Migrate(db)

	return &RouteTest{db, config, t}
}

func (t *RouteTest) TestEndpoint(test EndpointTest) (*httptest.ResponseRecorder, *http.Request) {
	r := httptest.NewRequest(test.Method, test.URL, test.Body)
	w := httptest.NewRecorder()

	if test.Token != nil {
		r.Header.Set("Authorization", *test.Token)
	}

	logrus.SetLevel(logrus.ErrorLevel)
	api := NewAPI(t.DB, t.Config)
	api.handler.ServeHTTP(w, r)

	return w, r
}
