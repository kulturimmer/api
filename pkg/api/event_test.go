package api

import (
	"fmt"
	"io"
	"net/http"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/kulturimmer/api/pkg/models"
)

type GetEventsTest struct {
	Description    string
	Code           int
	GenerateEvents bool
}

func TestGetEvents(t *testing.T) {
	type TestResponse struct {
		Data []models.Event `json:"data"`
	}

	testCases := []GetEventsTest{
		GetEventsTest{
			Description:    "no events",
			Code:           http.StatusOK,
			GenerateEvents: false,
		},
		GetEventsTest{
			Description:    "success data",
			Code:           http.StatusOK,
			GenerateEvents: true,
		},
	}

	test := NewRouteTest(t)

	for _, testCase := range testCases {
		if testCase.GenerateEvents {
			// create test data
			event := models.NewEvent("test event")
			res := test.DB.Create(event)
			assert.Nil(t, res.Error, testCase.Description)
		}

		w, _ := test.TestEndpoint(EndpointTest{
			Method: "GET",
			URL:    "http://localhost/events",
			Body:   nil,
			Token:  nil,
		})

		// test code
		assert.Equal(t, testCase.Code, w.Code, testCase.Description)
	}
}

type GetEventTest struct {
	Description string
	Code        int
}

func TestGetEvent(t *testing.T) {
	testCases := []GetEventTest{
		GetEventTest{
			Description: "id not found",
			Code:        http.StatusNotFound,
		},
		GetEventTest{
			Description: "success data",
			Code:        http.StatusOK,
		},
	}

	test := NewRouteTest(t)

	// create test data
	event := models.NewEvent("test event")
	res := test.DB.Create(event)
	assert.Nil(t, res.Error)

	for _, testCase := range testCases {
		id := event.ID

		if testCase.Code == http.StatusNotFound {
			id = "acf2c8c5-acb1-4611-93fe-65a8bebd25b4"
		}

		w, _ := test.TestEndpoint(EndpointTest{
			Method: "GET",
			URL:    fmt.Sprintf("http://localhost/events/%s", id),
			Body:   nil,
			Token:  nil,
		})

		// test code
		assert.Equal(t, testCase.Code, w.Code, testCase.Description)
	}
}

type CreateEventTest struct {
	Description string
	Code        int
	Token       string
	Payload     io.Reader
}

func TestCreateEvent(t *testing.T) {
	test := NewRouteTest(t)

	testCases := []CreateEventTest{
		CreateEventTest{
			Description: "no json",
			Code:        http.StatusBadRequest,
			Payload:     strings.NewReader(`asdasdasd`),
			Token:       test.Config.OperatorToken,
		},
		CreateEventTest{
			Description: "no payload",
			Code:        http.StatusBadRequest,
			Payload:     strings.NewReader(`{}`),
			Token:       test.Config.OperatorToken,
		},
		CreateEventTest{
			Description: "auth error",
			Code:        http.StatusUnauthorized,
			Payload:     strings.NewReader(`{"title":"My example Title","event_start":"2019-10-12T07:20:50.52Z"}`),
			Token:       "",
		},
		CreateEventTest{
			Description: "success data",
			Code:        http.StatusOK,
			Payload:     strings.NewReader(`{"title":"My example Title","event_start":"2019-10-12T07:20:50.52Z"}`),
			Token:       test.Config.OperatorToken,
		},
	}

	for _, testCase := range testCases {
		test.DB.Delete(models.Event{})

		w, _ := test.TestEndpoint(EndpointTest{
			Method: "POST",
			URL:    "http://localhost/events",
			Body:   testCase.Payload,
			Token:  &testCase.Token,
		})

		// test code
		assert.Equal(t, testCase.Code, w.Code, testCase.Description)
	}
}

type UpdateEventTest struct {
	Description string
	Code        int
	Token       string
	Payload     io.Reader
	ID          string
}

func TestUpdateEvent(t *testing.T) {
	test := NewRouteTest(t)

	// create test data
	event := models.NewEvent("test event")
	event.EventStart = time.Now()
	res := test.DB.Create(event)
	assert.Nil(t, res.Error)

	testCases := []UpdateEventTest{
		UpdateEventTest{
			Description: "no json",
			Code:        http.StatusBadRequest,
			Payload:     strings.NewReader(`asdasdasd`),
			Token:       test.Config.OperatorToken,
			ID:          event.ID,
		},
		UpdateEventTest{
			Description: "no payload",
			Code:        http.StatusBadRequest,
			Payload:     strings.NewReader(`{}`),
			Token:       test.Config.OperatorToken,
			ID:          event.ID,
		},
		UpdateEventTest{
			Description: "auth error",
			Code:        http.StatusUnauthorized,
			Payload:     strings.NewReader(`{"title":"My example Title","event_start":"2019-10-12T07:20:50.52Z"}`),
			Token:       "",
			ID:          event.ID,
		},
		UpdateEventTest{
			Description: "wrong id",
			Code:        http.StatusBadRequest,
			Payload:     strings.NewReader(`{"title":"My example Title","event_start":"2019-10-12T07:20:50.52Z"}`),
			Token:       test.Config.OperatorToken,
			ID:          "blalbalbla",
		},
		UpdateEventTest{
			Description: "not found",
			Code:        http.StatusNotFound,
			Payload:     strings.NewReader(`{"title":"My example Title","event_start":"2019-10-12T07:20:50.52Z"}`),
			Token:       test.Config.OperatorToken,
			ID:          "318789ab-181e-4ba1-bf41-be9d2912d465",
		},
		UpdateEventTest{
			Description: "success data",
			Code:        http.StatusOK,
			Payload:     strings.NewReader(`{"title":"My example Title 2","event_start":"2019-10-12T07:20:50.52Z"}`),
			Token:       test.Config.OperatorToken,
			ID:          event.ID,
		},
	}

	for _, testCase := range testCases {
		w, _ := test.TestEndpoint(EndpointTest{
			Method: "PUT",
			URL:    fmt.Sprintf("http://localhost/events/%s", testCase.ID),
			Body:   testCase.Payload,
			Token:  &testCase.Token,
		})

		// test code
		assert.Equal(t, testCase.Code, w.Code, testCase.Description)
	}
}
