package api

import (
	"context"
	"net/http"

	"github.com/go-chi/chi/middleware"
	"github.com/jinzhu/gorm"
	"github.com/rs/cors"
	logrus "github.com/sirupsen/logrus"

	"gitlab.com/kulturimmer/api/pkg/common/router"
	"gitlab.com/kulturimmer/api/pkg/config"
)

// API object
type API struct {
	handler    http.Handler
	router     *router.Router
	httpServer *http.Server
	db         *gorm.DB
	config     *config.Config
	log        *logrus.Entry
}

// NewAPI with routes
func NewAPI(db *gorm.DB, config *config.Config) *API {
	api := &API{
		db:     db,
		router: router.NewRouter(),
		config: config,
		log:    logrus.WithField("component", "api"),
	}

	// ======================================
	// Routes
	// ======================================
	api.log.Info("initialize Routes...")

	api.router.Route("/", func(r *router.Router) {
		r.UseBypass(middleware.RequestID)
		r.Use(router.Recoverer)
		r.Use(router.WithLogger(api.log))
		r.Use(api.withToken)

		r.UseBypass(middleware.Heartbeat("/health"))

		r.Route("/events", func(r *router.Router) {
			r.Get("/", api.getEvents)
			r.With(api.authToken).Post("/", api.createEvent)

			r.Route("/{eventID}", func(r *router.Router) {
				r.Use(api.withEvent)
				r.Get("/", api.getEvent)
				r.With(api.authToken).Put("/", api.updateEvent)
			})
		})
	})

	corsHandler := cors.New(cors.Options{
		AllowedMethods:   []string{"GET", "POST", "PATCH", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type"},
		ExposedHeaders:   []string{"Link", "X-Total-Count"},
		AllowCredentials: true,
	})
	api.handler = corsHandler.Handler(api.router)
	return api
}

// ListenAndServe api server
func (api *API) ListenAndServe(ctx context.Context, addr string) error {
	api.log.Info("Start App...")

	api.httpServer = &http.Server{
		Addr:    addr,
		Handler: api.handler,
	}

	return api.httpServer.ListenAndServe()
}

// Shutdown will shut down the API and its http server gracefully
func (api *API) Shutdown(ctx context.Context) {
	api.log.Info("api shutdown")

	if api.httpServer == nil {
		return
	}

	api.httpServer.Shutdown(ctx)
}
