package api

import (
	"context"
	"net/http"

	"gitlab.com/kulturimmer/api/pkg/common/router"
	session "gitlab.com/kulturimmer/api/pkg/context"
)

func (api *API) withToken(w http.ResponseWriter, r *http.Request) (context.Context, error) {
	ctx := r.Context()

	log := session.GetLogger(ctx)

	// extract token
	token := r.Header.Get("Authorization")
	if token == "" {
		return ctx, nil
	}

	if api.config.OperatorToken != "" && token == api.config.OperatorToken {
		log.Info("is operator")
		ctx = session.SetOperator(ctx)

		ctx = router.LoggerSetField(ctx, "isOperator", true)

		return ctx, nil
	}

	return ctx, nil
}

func (api *API) authToken(w http.ResponseWriter, r *http.Request) (context.Context, error) {
	ctx := r.Context()

	if session.IsOperator(ctx) {
		return ctx, nil
	}

	return nil, router.UnauthorizedError("need auth")
}
