package router

import (
	"context"
	"net/http"
	"time"

	"github.com/go-chi/chi/middleware"
	logrus "github.com/sirupsen/logrus"
)

// WithLogger add metadata to log
func WithLogger(log *logrus.Entry) func(w http.ResponseWriter, r *http.Request) (context.Context, error) {
	return func(w http.ResponseWriter, r *http.Request) (context.Context, error) {
		ctx := r.Context()

		logFields := logrus.Fields{
			"method":      r.Method,
			"path":        r.URL.Path,
			"remote_addr": r.RemoteAddr,
			"timestamp":   time.Now().UTC().Format(time.RFC1123),
		}

		if reqID := middleware.GetReqID(ctx); reqID != "" {
			logFields["request_id"] = reqID
		}

		requestLogger := log.WithFields(logFields)

		ctx = SetLogger(ctx, requestLogger)

		return ctx, nil
	}
}

// SetLogger to context
func SetLogger(ctx context.Context, requestLogger *logrus.Entry) context.Context {
	return context.WithValue(ctx, middleware.LogEntryCtxKey, requestLogger)
}

// GetLogger from context
func GetLogger(ctx context.Context) *logrus.Entry {
	logger := ctx.Value(middleware.LogEntryCtxKey)

	if logger == nil {
		return logrus.WithField("log_error", "logger not found")
	}

	return logger.(*logrus.Entry)
}

// LoggerSetField and overwrite the logger entry
func LoggerSetField(ctx context.Context, key string, value interface{}) context.Context {
	logger := GetLogger(ctx)
	logger = logger.WithField(key, value)

	return SetLogger(ctx, logger)
}

// LoggerSetFields and overwrite the logger entry
func LoggerSetFields(ctx context.Context, fields logrus.Fields) context.Context {
	logger := GetLogger(ctx)
	logger = logger.WithFields(fields)

	return SetLogger(ctx, logger)
}
