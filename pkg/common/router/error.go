package router

import (
	"fmt"
	"net/http"

	"github.com/go-chi/chi/middleware"
	"github.com/go-sql-driver/mysql"
)

// BadRequestError return bad request error
func BadRequestError(fmtString string, args ...interface{}) *HTTPError {
	return NewHTTPError(http.StatusBadRequest, fmtString, args...)
}

// InternalServerError return internal server error
func InternalServerError(fmtString string, args ...interface{}) *HTTPError {
	return NewHTTPError(http.StatusInternalServerError, fmtString, args...)
}

// NotFoundError return not found error
func NotFoundError(fmtString string, args ...interface{}) *HTTPError {
	return NewHTTPError(http.StatusNotFound, fmtString, args...)
}

// UnauthorizedError return unauthorized error
func UnauthorizedError(fmtString string, args ...interface{}) *HTTPError {
	return NewHTTPError(http.StatusUnauthorized, fmtString, args...)
}

// UnavailableServiceError return unavailable service error
func UnavailableServiceError(fmtString string, args ...interface{}) *HTTPError {
	return NewHTTPError(http.StatusServiceUnavailable, fmtString, args...)
}

// HandleSQLError handle sql errors as internal server error
func HandleSQLError(err error) error {
	if mysqlError, ok := err.(*mysql.MySQLError); ok {
		if mysqlError.Number == 1062 {
			return BadRequestError("duplicate slug").WithInternalError(mysqlError)
		}
	}

	return InternalServerError("internal server error").WithInternalError(err)
}

// HTTPError is an error with a message and an HTTP status code.
type HTTPError struct {
	Code            int         `json:"error"`
	Message         string      `json:"msg"`
	JSON            interface{} `json:"json,omitempty"`
	InternalError   error       `json:"-"`
	InternalMessage string      `json:"-"`
	ErrorID         string      `json:"error_id,omitempty"`
}

// Error return error as string
func (e *HTTPError) Error() string {
	if e.InternalMessage != "" {
		return e.InternalMessage
	}
	return fmt.Sprintf("%d: %s", e.Code, e.Message)
}

// Cause returns the root cause error
func (e *HTTPError) Cause() error {
	if e.InternalError != nil {
		return e.InternalError
	}
	return e
}

// WithJSONError add a json to error
func (e *HTTPError) WithJSONError(json interface{}) *HTTPError {
	e.JSON = json
	return e
}

// WithInternalError adds internal error information to the error
func (e *HTTPError) WithInternalError(err error) *HTTPError {
	e.InternalError = err
	return e
}

// WithInternalMessage adds internal message information to the error
func (e *HTTPError) WithInternalMessage(fmtString string, args ...interface{}) *HTTPError {
	e.InternalMessage = fmt.Sprintf(fmtString, args...)
	return e
}

// NewHTTPError create a new http error
func NewHTTPError(code int, fmtString string, args ...interface{}) *HTTPError {
	return &HTTPError{
		Code:    code,
		Message: fmt.Sprintf(fmtString, args...),
	}
}

// HandleError handles all errors
func HandleError(err error, w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	type errorResponse struct {
		Error *HTTPError `error`
	}

	log := GetLogger(ctx)
	errorID := middleware.GetReqID(r.Context())
	switch e := err.(type) {
	case *HTTPError:
		if e.Code >= http.StatusInternalServerError {
			e.ErrorID = errorID
			// this will get us the stack trace too
			log.WithError(e.Cause()).Error(e.Error())
		} else {
			log.WithError(e.Cause()).Info(e.Error())
		}
		if jsonErr := SendJSON(w, e.Code, errorResponse{e}); jsonErr != nil {
			HandleError(jsonErr, w, r)
		}
	default:
		log.WithError(e).Errorf("Unhandled server error: %s", e.Error())
		// hide real error details from response to prevent info leaks
		w.WriteHeader(http.StatusInternalServerError)
		if _, writeErr := w.Write([]byte(`{"error": {"code":500,"msg":"Internal server error","error_id":"` + errorID + `"}}`)); writeErr != nil {
			log.WithError(writeErr).Error("Error writing generic error message")
		}
	}
}
