package router

import (
	"context"
	"net/http"

	"github.com/go-chi/chi"
)

// Router wrapper to handle automatic error response
type Router struct {
	chi chi.Router
}

// NewRouter create new router wrapper
func NewRouter() *Router {
	return &Router{chi.NewRouter()}
}

// Route mounts a sub-Router along a `pattern`` string.
func (r *Router) Route(pattern string, fn func(*Router)) {
	r.chi.Route(pattern, func(c chi.Router) {
		fn(&Router{c})
	})
}

// Get HTTP-method routing along `pattern`
func (r *Router) Get(pattern string, fn apiHandler) {
	r.chi.Get(pattern, errorWrapper(fn))
}

// Post HTTP-method routing along `pattern`
func (r *Router) Post(pattern string, fn apiHandler) {
	r.chi.Post(pattern, errorWrapper(fn))
}

// Put HTTP-method routing along `pattern`
func (r *Router) Put(pattern string, fn apiHandler) {
	r.chi.Put(pattern, errorWrapper(fn))
}

// Delete HTTP-method routing along `pattern`
func (r *Router) Delete(pattern string, fn apiHandler) {
	r.chi.Delete(pattern, errorWrapper(fn))
}

// Sse HTTP-method routing along `pattern`
func (r *Router) Sse(pattern string, fn func(w http.ResponseWriter, r *http.Request)) {
	r.chi.Get(pattern, fn)
}

// With adds inline middlewares for an endpoint handler.
func (r *Router) With(fn middlewareHandler) *Router {
	c := r.chi.With(middlewareWrapper(fn))
	return &Router{c}
}

// WithBypass adds inline middlewares with go-chi signature (no automatic error response)
func (r *Router) WithBypass(fn func(next http.Handler) http.Handler) *Router {
	c := r.chi.With(fn)
	return &Router{c}
}

// Use appends one of more middlewares onto the Router stack.
func (r *Router) Use(fn middlewareHandler) {
	r.chi.Use(middlewareWrapper(fn))
}

// UseBypass appends one of more middlewares with go-chi signature (no automatic error response)
func (r *Router) UseBypass(fn func(next http.Handler) http.Handler) {
	r.chi.Use(fn)
}

// ServeHTTP wrap the chi function
func (r *Router) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	r.chi.ServeHTTP(w, req)
}

// apiHandler forms a controller function type
type apiHandler func(w http.ResponseWriter, r *http.Request) error

// errorWrapper wrap the handler function
func errorWrapper(fn apiHandler) http.HandlerFunc {
	return fn.serve
}

// serve inject the error handler
func (h apiHandler) serve(w http.ResponseWriter, r *http.Request) {
	if err := h(w, r); err != nil {
		HandleError(err, w, r)
	}
}

// middlewareHandler forms a custom middleware function type
type middlewareHandler func(w http.ResponseWriter, r *http.Request) (context.Context, error)

// handler wrap the handler function
func (m middlewareHandler) handler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		m.serve(next, w, r)
	})
}

// serve inject the error handler
func (m middlewareHandler) serve(next http.Handler, w http.ResponseWriter, r *http.Request) {
	ctx, err := m(w, r)
	if err != nil {
		HandleError(err, w, r)
		return
	}
	if ctx != nil {
		r = r.WithContext(ctx)
	}
	next.ServeHTTP(w, r)
}

// middlewareWrapper wrap the handler function
func middlewareWrapper(fn middlewareHandler) func(http.Handler) http.Handler {
	return fn.handler
}
