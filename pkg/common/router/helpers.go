package router

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/pkg/errors"
)

// SendList with data wrapper
func SendList(w http.ResponseWriter, status int, obj interface{}) error {
	type Response struct {
		Data interface{} `json:"data"`
	}

	return SendJSON(w, status, Response{
		Data: obj,
	})
}

// SendObject with data wrapper
func SendObject(w http.ResponseWriter, status int, obj interface{}) error {
	type Response struct {
		Data interface{} `json:"data"`
	}

	return SendJSON(w, status, Response{
		Data: obj,
	})
}

// SendJSON send response as json
func SendJSON(w http.ResponseWriter, status int, obj interface{}) error {
	w.Header().Set("Content-Type", "application/json")
	b, err := json.Marshal(obj)
	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("Error encoding json response: %v", obj))
	}
	w.WriteHeader(status)
	_, err = w.Write(b)
	return err
}

// QueryParamInt get a int query parameter from url
func QueryParamInt(r *http.Request, key string) int {
	param := r.URL.Query().Get(key)

	if param == "" {
		return 0
	}

	val, err := strconv.Atoi(param)
	if err != nil {
		return 0
	}

	return val
}

// QueryParamString get a int query parameter from url
func QueryParamString(r *http.Request, key string) string {
	param := r.URL.Query().Get(key)

	if param == "" {
		return ""
	}

	return param
}
