package router

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"runtime/debug"
)

// Recoverer is a middleware that recovers from panics, logs the panic (and a
// backtrace), and returns a HTTP 500 (Internal Server Error) status if
// possible. Recoverer prints a request ID if one is provided.
func Recoverer(w http.ResponseWriter, r *http.Request) (context.Context, error) {
	ctx := r.Context()

	defer func() {
		if rvr := recover(); rvr != nil {

			log := GetLogger(ctx)
			if log != nil {
				log.Panic(rvr, debug.Stack())
			} else {
				fmt.Fprintf(os.Stderr, "Panic: %+v\n", rvr)
				debug.PrintStack()
			}

			se := &HTTPError{
				Code:    http.StatusInternalServerError,
				Message: http.StatusText(http.StatusInternalServerError),
			}
			HandleError(se, w, r)
		}
	}()

	return nil, nil
}
