package context

import (
	"context"

	"github.com/go-chi/chi/middleware"
	logrus "github.com/sirupsen/logrus"

	"gitlab.com/kulturimmer/api/pkg/common/router"
	"gitlab.com/kulturimmer/api/pkg/models"
)

type contextKey string

const (
	eventKey    = contextKey("event")
	operatorKey = contextKey("operator")
)

// GetLogger return a log object from context
func GetLogger(ctx context.Context) *logrus.Entry {
	return router.GetLogger(ctx)
}

// GetRequestID return a request id from context
func GetRequestID(ctx context.Context) string {
	return middleware.GetReqID(ctx)
}

// WithEvent add a event to context
func WithEvent(ctx context.Context, event *models.Event) context.Context {
	ctx = context.WithValue(ctx, eventKey, event)

	return ctx
}

// GetEvent return the event from context
func GetEvent(ctx context.Context) *models.Event {
	event := ctx.Value(eventKey)

	if event == nil {
		return nil
	}

	return event.(*models.Event)
}

// SetOperator add a operator status to context
func SetOperator(ctx context.Context) context.Context {
	ctx = context.WithValue(ctx, operatorKey, true)

	return ctx
}

// IsOperator return the operator status from context
func IsOperator(ctx context.Context) bool {
	isOperator := ctx.Value(operatorKey)

	return isOperator != nil
}
