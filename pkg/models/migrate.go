package models

import (
	"time"

	"github.com/jinzhu/gorm"
)

func timeParse(timeString string) time.Time {
	t, _ := time.Parse(time.RFC3339, timeString)
	return t
}

// Migrate the database
func Migrate(db *gorm.DB) error {
	if res := db.AutoMigrate(&Event{}); res.Error != nil {
		return res.Error
	}

	// delete all data
	db.Unscoped().Delete(Event{})

	// seed demo data
	seedData := []Event{
		Event{
			Title:         "Promis laden zum Online-Konzert ein",
			Description:   "Hashtag #wirbleibenzuhausefestival; Online Konzert von mehreren Künstlern: Max Giesinger, Lotte, Nico Santos und Álvaro Soler ",
			EventStart:    timeParse("2020-03-22T18:00:00.0000000Z"),
			WithStartTime: true,
			Platform:      "Instagram",
			URL:           "https://www.general-anzeiger-bonn.de/ratgeber/kindernachrichten/promis-laden-zum-online-konzert-ein_aid-49666129",
			Category:      "Musik",
		},
		Event{
			Title:         "Biennale 2020",
			Description:   "Virtueller Durchgang durch ein Museum ",
			EventStart:    timeParse("2020-02-29T18:00:00.0000000Z"),
			WithStartTime: false,
			Platform:      "Webseite",
			URL:           "https://biennalefotografie.de/edition/virtueller-rundgang/all-art-is-photography#",
			Category:      "Kreativität",
		},
		Event{
			Title:         "Rolling Stone Online Konzerte",
			Description:   "`in my room` Konzerte ",
			EventStart:    timeParse("2020-03-25T20:00:00.0000000Z"),
			WithStartTime: true,
			Platform:      "Instagram",
			URL:           "https://www.instagram.com/rollingstone/",
			Category:      "Musik",
		},
		Event{
			Title:         "Online Probe für ein Chormusical",
			Description:   "Online Musical Proben für das Chromusical `bethlehem`",
			EventStart:    timeParse("2020-03-24T19:30:00.0000000Z"),
			WithStartTime: true,
			Platform:      "Webseite",
			URL:           "https://www.instagram.com/rollingstone/",
			Category:      "Musik",
		},
		Event{
			Title:         "Spotlight Werbefilmfestival",
			Description:   "Publikumabstimmung zur Wahl des Besten Werbespots des Jahres",
			EventStart:    timeParse("2020-03-24T22:00:00.0000000Z"),
			WithStartTime: true,
			Platform:      "Webseite",
			URL:           "https://www.spotlight-festival.de",
			Category:      "Kreativität",
		},
		Event{
			Title:         "Lesung von Saša Stanišić",
			Description:   "Publikumabstimmung zur Wahl des Besten Werbespots des Jahres",
			EventStart:    timeParse("2020-03-31T00:00:00.0000000Z"),
			WithStartTime: false,
			Platform:      "Twitch",
			URL:           "https://www.twitch.tv/sasastanisic",
			Category:      "Literatur",
		},
		Event{
			Title:         "Lesung von Schauspielstudenten: Designiert euch",
			Description:   "Lesung von Schauspielstudenten: Designiert euch",
			EventStart:    timeParse("2020-03-21T00:00:00.0000000Z"),
			WithStartTime: false,
			Platform:      "Instagram",
			URL:           "https://www.twitch.tv/sasastanisic",
			Category:      "Literatur",
		},
		Event{
			Title:         "Homeworkout zum Mitmachen für Jedermann",
			Description:   "Live Homeworkout zum Mitmachen für Jedermann",
			EventStart:    timeParse("2020-03-21T12:00:00.0000000Z"),
			WithStartTime: true,
			Platform:      "Twitch",
			URL:           "https://www.twitch.tv/ginoswelt",
			Category:      "Sport",
		},
		Event{
			Title:         "Homeworkout zum Mitmachen für Jedermann",
			Description:   "Live Homeworkout zum Mitmachen für Jedermann",
			EventStart:    timeParse("2020-03-22T12:00:00.0000000Z"),
			WithStartTime: true,
			Platform:      "Twitch",
			URL:           "https://www.twitch.tv/ginoswelt",
			Category:      "Sport",
		},
	}

	for _, event := range seedData {
		db.Create(&event)
	}

	return nil
}
