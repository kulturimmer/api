package models

import (
	"time"

	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
)

// Event Model
type Event struct {
	ID string `gorm:"type:uuid; primary_key;" json:"id" validate:"-"`

	Title       string `json:"title" validate:"required,min=5,max=200,alphanum"`
	Description string `json:"description" validate:"omitempty,max=200,alphanum"`

	ImageURL string `json:"image_url" validate:"omitempty,url"`

	EventStart    time.Time `json:"event_start" validate:"required"`
	WithStartTime bool      `json:"with_starttime"  validate:"-"`

	Platform string `json:"platform" validate:"omitempty,max=40,alphanum"`
	URL      string `json:"url" validate:"omitempty,url"`

	Category string `json:"category" validate:"omitempty,max=40,alphanum"`

	CreatedAt time.Time  `json:"created_at" validate:"-"`
	UpdatedAt time.Time  `json:"updated_at" validate:"-"`
	DeletedAt *time.Time `sql:"index" json:"deleted_at,omitempty" validate:"-"`
}

// NewEvent return a event object with title
func NewEvent(title string) *Event {
	return &Event{
		Title: title,
	}
}

// BeforeCreate will set a UUID rather than numeric ID.
func (e *Event) BeforeCreate(scope *gorm.Scope) error {
	scope.SetColumn("ID", uuid.New().String())

	return nil
}
