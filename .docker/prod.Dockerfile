ARG VERSION=1.14
FROM golang:$VERSION as builder

WORKDIR /src
ENV GO111MODULE=on

COPY ./go.mod ./go.sum ./
RUN go mod download

COPY . .

RUN CGO_ENABLED=0 go build -ldflags '-extldflags "-static"' -o /release/kulturimmer
RUN cd /release && ls -aGlhSr

FROM gcr.io/distroless/base as runner
COPY --from=builder /release/kulturimmer /

CMD ["/kulturimmer", "-m"]
