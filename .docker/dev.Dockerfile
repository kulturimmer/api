ARG VERSION=1.14
FROM golang:$VERSION as builder

WORKDIR /src
ENV GO111MODULE=on

RUN go get -u github.com/maxcnunes/gaper/cmd/gaper

COPY ./go.mod ./go.sum ./
RUN go mod download

COPY . .

RUN ls -all
