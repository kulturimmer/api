# Usage

## CLI

```bash
go build -o ./kulturimmer

kulturimmer -m
```

## Configuration

Operator Auth:

- `OPERATOR_TOKEN=supersecret`

Postgres:

- `DATABASE_DRIVER=postgres`
- `DATABASE_PATH=host=postgres port=5432 user=kulturimmer dbname=kulturimmer password=example sslmode=disable`

# API

## GET `/events`

Returns all events.

**Optional Parameters**

- **per_page** events per page
- **page** page number
- **start** start date (end date is optional) `2020-01-28`
- **end** end date `2020-01-28`

Example: `/events?perPage=20&page=5&start=2020-01-01&end=2020-01-28`

```json
{
  "data": [
    {
      "id": "0a139955-7bea-4706-8645-38a012c97bd5",
      "title": "Promis laden zum Online-Konzert ein",
      "description": "Hashtag #wirbleibenzuhausefestival; Online Konzert von mehreren Künstlern: Max Giesinger, Lotte, Nico Santos und Álvaro Soler ",
      "image_url": "",
      "event_start": "0001-01-01T00:00:00Z",
      "with_starttime": true,
      "plattform": "Instagram",
      "url": "https://www.general-anzeiger-bonn.de/ratgeber/kindernachrichten/promis-laden-zum-online-konzert-ein_aid-49666129",
      "category": "Musik",
      "created_at": "2020-03-21T23:19:23.888002Z",
      "updated_at": "2020-03-21T23:19:23.888002Z"
    }
  ]
}
```

## GET `/events/{ID}`

Returns a specific event by its UUID.

```json
{
  "data": {
    "id": "0a139955-7bea-4706-8645-38a012c97bd5",
    "title": "Promis laden zum Online-Konzert ein",
    "description": "Hashtag #wirbleibenzuhausefestival; Online Konzert von mehreren Künstlern: Max Giesinger, Lotte, Nico Santos und Álvaro Soler ",
    "image_url": "",
    "event_start": "0001-01-01T00:00:00Z",
    "with_starttime": true,
    "plattform": "Instagram",
    "url": "https://www.general-anzeiger-bonn.de/ratgeber/kindernachrichten/promis-laden-zum-online-konzert-ein_aid-49666129",
    "category": "Musik",
    "created_at": "2020-03-21T23:19:23.888002Z",
    "updated_at": "2020-03-21T23:19:23.888002Z"
  }
}
```

## POST `/events`

Create a new Event.

**Auth:**

Header: `Authorization`: `operator token`

**Payload:**

```json
{
	"title": "Promis laden zum Online-Konzert ein",
	"description": "Hashtag #wirbleibenzuhausefestival; Online Konzert von mehreren Künstlern: Max Giesinger, Lotte, Nico Santos und Álvaro Soler ",
	"image_url": "",
	"event_start": "2020-03-21T23:19:23.888002Z",
	"with_starttime": true,
	"plattform": "Instagram",
	"url": "https://www.general-anzeiger-bonn.de/ratgeber/kindernachrichten/promis-laden-zum-online-konzert-ein_aid-49666129",
	"category": "Musik"
}
```

**Response:**

```json
{
  "data": {
    "id": "48e7ef4a-0738-4327-aa6a-079cf161fd71",
    "title": "Promis laden zum Online-Konzert ein",
    "description": "Hashtag #wirbleibenzuhausefestival; Online Konzert von mehreren Künstlern: Max Giesinger, Lotte, Nico Santos und Álvaro Soler ",
    "image_url": "",
    "event_start": "2020-03-21T23:19:23.888002Z",
    "with_starttime": true,
    "plattform": "Instagram",
    "url": "https://www.general-anzeiger-bonn.de/ratgeber/kindernachrichten/promis-laden-zum-online-konzert-ein_aid-49666129",
    "category": "Musik",
    "created_at": "2020-03-21T23:26:32.2158848Z",
    "updated_at": "2020-03-21T23:26:32.2158848Z"
  }
}
```

## PUT `/events/{ID}`

Update / Overwrite a Event.

**Auth:**

Header: `Authorization`: `operator token`

**Payload:**

```json
{
	"title": "Promis laden zum Online-Konzert ein",
	"description": "Hashtag #wirbleibenzuhausefestival; Online Konzert von mehreren Künstlern: Max Giesinger, Lotte, Nico Santos und Álvaro Soler ",
	"image_url": "",
	"event_start": "2020-03-21T23:19:23.888002Z",
	"with_starttime": true,
	"plattform": "Instagram",
	"url": "https://www.general-anzeiger-bonn.de/ratgeber/kindernachrichten/promis-laden-zum-online-konzert-ein_aid-49666129",
	"category": "Musik"
}
```

**Response:**

```json
{
  "data": {
    "id": "0a139955-7bea-4706-8645-38a012c97bd5",
    "title": "Promis laden zum Online-Konzert ein",
    "description": "Hashtag #wirbleibenzuhausefestival; Online Konzert von mehreren Künstlern: Max Giesinger, Lotte, Nico Santos und Álvaro Soler ",
    "image_url": "",
    "event_start": "2020-03-21T23:19:23.888002Z",
    "with_starttime": true,
    "plattform": "Instagram",
    "url": "https://www.general-anzeiger-bonn.de/ratgeber/kindernachrichten/promis-laden-zum-online-konzert-ein_aid-49666129",
    "category": "Musik",
    "created_at": "2020-03-21T23:19:23.888002Z",
    "updated_at": "2020-03-21T23:25:33.636151Z"
  }
}
```

# Deployment

- Clone the repositiory
- Run `docker-compose up`
- Runs all with hot-relaod

# License

MIT
