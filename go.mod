module gitlab.com/kulturimmer/api

go 1.14

require (
	github.com/Netflix/go-env v0.0.0-20200312172415-986dfe862277
	github.com/go-chi/chi v4.0.3+incompatible
	github.com/go-playground/validator/v10 v10.2.0
	github.com/go-sql-driver/mysql v1.4.1
	github.com/google/uuid v1.1.1
	github.com/jinzhu/gorm v1.9.12
	github.com/pkg/errors v0.8.0
	github.com/rs/cors v1.7.0
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v0.0.6
	github.com/stretchr/testify v1.4.0
)
