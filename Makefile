PROJECT = kulturimmer
APP = api

.PHONY: run
run:
	go build -o ./$(PROJECT)-$(APP) && DATABASE_DRIVER=sqlite3 DATABASE_PATH=data.db ./$(PROJECT)-$(APP) -m
