package cmd

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"gitlab.com/kulturimmer/api/pkg/config"
)

var executeMigrate = false

// rootCmd will run the log streamer
var rootCmd = cobra.Command{
	Use:  "kulturimmer",
	Long: "A service that will serve a restufl event list service",
	Run: func(cmd *cobra.Command, args []string) {
		execWithConfig(cmd, serve)
	},
}

// RootCmd will add flags and subcommands to the different commands
func RootCmd() *cobra.Command {
	rootCmd.PersistentFlags().BoolVarP(&executeMigrate, "migrate", "m", false, "migrate database")
	rootCmd.AddCommand(&serveCmd)
	return &rootCmd
}

func execWithConfig(cmd *cobra.Command, fn func(config *config.Config)) {
	logrus.Info("Read Config...")
	config := config.ReadConfig("config.json")

	fn(config)
}
