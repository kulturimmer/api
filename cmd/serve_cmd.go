package cmd

import (
	"context"
	"os"
	"os/signal"
	"syscall"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"github.com/jinzhu/gorm"

	"gitlab.com/kulturimmer/api/pkg/api"
	"gitlab.com/kulturimmer/api/pkg/config"
	"gitlab.com/kulturimmer/api/pkg/models"
)

var serveCmd = cobra.Command{
	Use:   "serve",
	Short: "Serve API",
	Long:  "Serve the service api",
	Run: func(cmd *cobra.Command, args []string) {
		execWithConfig(cmd, serve)
	},
}

func serve(config *config.Config) {
	log.SetFormatter(&log.TextFormatter{
		DisableColors: false,
		FullTimestamp: true,
	})

	// ======================================
	// Graceful Shutdown
	// ======================================
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt, syscall.SIGTERM, syscall.SIGINT)
		sig := <-c
		log.Infof("Received Signal %s. Shutting down.", sig)
		cancel()
	}()

	// ======================================
	// Database
	// ======================================
	log.Info("Init Database...")

	var db *gorm.DB
	var err error
	for {
		db, err = gorm.Open(config.Database.Driver, config.Database.Path)
		if err != nil {
			log.Error("try to connect...")
		} else {
			break
		}
		time.Sleep(5 * time.Second)
	}
	defer db.Close()
	log.Info("Database connected!")

	if executeMigrate {
		log.Info("Migrate...")
		models.Migrate(db)
	}

	// ======================================
	// Server
	// ======================================
	server := api.NewAPI(db, config)

	go func() {
		if err := server.ListenAndServe(ctx, ":8080"); err != nil {
			cancel()
		}
	}()

	<-ctx.Done()

	ctx, cancel = context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	server.Shutdown(ctx)
}
